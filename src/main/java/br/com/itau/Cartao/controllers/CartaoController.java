package br.com.itau.Cartao.controllers;

import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.models.DTO.AlterarAtivoEntradaDTO;
import br.com.itau.Cartao.models.DTO.CartaoEntradaDTO;
import br.com.itau.Cartao.models.DTO.CartaoSaidaDTO;
import br.com.itau.Cartao.models.DTO.CartaoSaidaGetDTO;
import br.com.itau.Cartao.models.mapper.CartaoMapper;
import br.com.itau.Cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaidaDTO criaCartao(@RequestBody @Valid CartaoEntradaDTO cartaoEntradaDTO) {
        try {
            Cartao cartao = cartaoService.criarCartao(cartaoEntradaDTO);
            return CartaoMapper.toCartaoSaidaDTO(cartao);
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public CartaoSaidaDTO alterarAtivo(@PathVariable String numero,
                                       @RequestBody AlterarAtivoEntradaDTO alterarAtivoEntradaDTO) {
        try {
            Cartao cartao = cartaoService.alterarStatus(numero, alterarAtivoEntradaDTO);
            return CartaoMapper.toCartaoSaidaDTO(cartao);
        } catch (RuntimeException re){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }

    }

    @GetMapping("/{numero}")
    public CartaoSaidaGetDTO buscarCartao(@PathVariable String numero){
        try {
            Cartao cartao = cartaoService.buscarCartao(numero);
            return CartaoMapper.toCartaoSaidaGetDTO(cartao);
        } catch (RuntimeException re){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }
}
