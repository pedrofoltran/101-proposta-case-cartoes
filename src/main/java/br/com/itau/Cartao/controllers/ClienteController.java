package br.com.itau.Cartao.controllers;

import br.com.itau.Cartao.models.Cliente;
import br.com.itau.Cartao.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criarCliente(@RequestBody Cliente cliente){
        return clienteService.criarCliente(cliente);
    }

    @GetMapping("/{id}")
    public Cliente buscarClientePorID(@PathVariable(name = "id") int id){
        try {
            return clienteService.buscarClientePorID(id);
        } catch (RuntimeException re){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }

    }

}
