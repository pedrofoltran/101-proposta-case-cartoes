package br.com.itau.Cartao.controllers;

import br.com.itau.Cartao.models.DTO.PagamentoEntradaDTO;
import br.com.itau.Cartao.models.DTO.PagamentoSaidaDTO;
import br.com.itau.Cartao.models.Pagamento;
import br.com.itau.Cartao.models.mapper.PagamentoMapper;
import br.com.itau.Cartao.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController

public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoSaidaDTO pagamentoItem(@RequestBody @Valid PagamentoEntradaDTO pagamentoEntradaDTO) {
        try {
            Pagamento pagamento = pagamentoService.pagarItem(pagamentoEntradaDTO);
            return PagamentoMapper.toPagamentoSaidaDTO(pagamento);
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }

    @GetMapping("pagamentos/{id}")
    public List<PagamentoSaidaDTO> pagamentosCartao(@PathVariable int id) {
        try {
            List<PagamentoSaidaDTO> pagamentoSaidaDTOList = new ArrayList<>();
            List<Pagamento> pagamentoList = pagamentoService.listarPagamentos(id);

            for (Pagamento pagamento : pagamentoList) {
                PagamentoSaidaDTO pagamentoSaidaDTO = PagamentoMapper.toPagamentoSaidaDTO(pagamento);
                pagamentoSaidaDTOList.add(pagamentoSaidaDTO);
            }

            return pagamentoSaidaDTOList;
        } catch (RuntimeException re) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, re.getMessage());
        }
    }

}
