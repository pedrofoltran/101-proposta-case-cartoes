package br.com.itau.Cartao.models.DTO;

public class AlterarAtivoEntradaDTO {
    public boolean ativo;

    public AlterarAtivoEntradaDTO() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
