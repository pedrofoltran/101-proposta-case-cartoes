package br.com.itau.Cartao.models.DTO;

public class CartaoEntradaDTO {
    private String numero;
    private int clienteId;

    public CartaoEntradaDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
