package br.com.itau.Cartao.models.DTO;

public class PagamentoEntradaDTO {
    private int cartao_id;
    private String descricao;
    private double valor;

    public PagamentoEntradaDTO() {
    }

    public PagamentoEntradaDTO(int cartao_id, String descricao, double valor) {
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
