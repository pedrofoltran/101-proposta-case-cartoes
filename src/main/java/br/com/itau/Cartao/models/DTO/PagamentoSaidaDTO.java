package br.com.itau.Cartao.models.DTO;

import java.math.BigDecimal;

public class PagamentoSaidaDTO {
    private int id;
    private int cartao_id;
    private String descricao;
    private BigDecimal valor;

    public PagamentoSaidaDTO() {
    }

    public PagamentoSaidaDTO(int id, int cartao_id, String descricao, BigDecimal valor) {
        this.id = id;
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
