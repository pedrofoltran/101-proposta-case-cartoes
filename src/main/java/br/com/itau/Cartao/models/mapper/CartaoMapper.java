package br.com.itau.Cartao.models.mapper;

import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.models.DTO.CartaoSaidaDTO;
import br.com.itau.Cartao.models.DTO.CartaoSaidaGetDTO;

public class CartaoMapper {
    public static CartaoSaidaDTO toCartaoSaidaDTO(Cartao cartao) {
        return new CartaoSaidaDTO(cartao.getId(),
                cartao.getNumero(),
                cartao.getClienteId().getId(),
                cartao.getAtivo());
    }

    public static CartaoSaidaGetDTO toCartaoSaidaGetDTO(Cartao cartao) {
        return new CartaoSaidaGetDTO(cartao.getId(),
                cartao.getNumero(),
                cartao.getClienteId().getId());
    }

}
