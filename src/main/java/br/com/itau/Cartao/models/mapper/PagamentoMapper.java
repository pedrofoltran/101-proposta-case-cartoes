package br.com.itau.Cartao.models.mapper;

import br.com.itau.Cartao.models.DTO.PagamentoSaidaDTO;
import br.com.itau.Cartao.models.Pagamento;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PagamentoMapper {
    public static PagamentoSaidaDTO toPagamentoSaidaDTO(Pagamento pagamento){
        return new PagamentoSaidaDTO(pagamento.getId(),
                pagamento.getCartao().getId(),
                pagamento.getDescricao(),
                new BigDecimal(pagamento.getValor()).setScale(2, RoundingMode.HALF_UP));
    }
}
