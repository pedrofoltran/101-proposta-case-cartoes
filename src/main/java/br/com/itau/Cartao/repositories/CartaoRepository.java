package br.com.itau.Cartao.repositories;

import br.com.itau.Cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findByNumero(String numero);
}
