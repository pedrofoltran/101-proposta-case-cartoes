package br.com.itau.Cartao.repositories;

import br.com.itau.Cartao.models.Cliente;
import org.springframework.data.repository.CrudRepository;


public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
