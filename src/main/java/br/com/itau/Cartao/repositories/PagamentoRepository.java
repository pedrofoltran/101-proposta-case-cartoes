package br.com.itau.Cartao.repositories;

import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    List<Pagamento> findAllByCartao(Cartao cartao);
}
