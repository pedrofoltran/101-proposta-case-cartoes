package br.com.itau.Cartao.services;

import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.models.Cliente;
import br.com.itau.Cartao.models.DTO.AlterarAtivoEntradaDTO;
import br.com.itau.Cartao.models.DTO.CartaoEntradaDTO;
import br.com.itau.Cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;
    @Autowired
    ClienteService clienteService;

    public Cartao criarCartao(@Valid CartaoEntradaDTO cartaoEntradaDTO) {
        Cartao cartao = new Cartao();
        Cliente cliente = clienteService.buscarClientePorID(cartaoEntradaDTO.getClienteId());

        cartao.setNumero(cartaoEntradaDTO.getNumero());
        cartao.setClienteId(cliente);
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao alterarStatus(String numero, AlterarAtivoEntradaDTO alterarAtivoEntradaDTO) {
        Cartao cartao = buscarCartao(numero);
        cartao.setAtivo(alterarAtivoEntradaDTO.isAtivo());
        return cartaoRepository.save(cartao);
    }

    public Cartao buscarCartao(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent())
            return cartaoOptional.get();
        else
            throw new RuntimeException("Cartao nao encontrado");
    }

    public Cartao buscarCartao(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if (cartaoOptional.isPresent())
            return cartaoOptional.get();
        else
            throw new RuntimeException("Cartao nao encontrado");
    }
}
