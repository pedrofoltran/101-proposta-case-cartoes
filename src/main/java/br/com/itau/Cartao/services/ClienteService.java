package br.com.itau.Cartao.services;

import br.com.itau.Cartao.models.Cliente;
import br.com.itau.Cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorID(int id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if (optionalCliente.isPresent())
            return optionalCliente.get();
        else
            throw new RuntimeException("Cliente nao encontrado");
    }
}
