package br.com.itau.Cartao.services;

import br.com.itau.Cartao.models.Cartao;
import br.com.itau.Cartao.models.DTO.PagamentoEntradaDTO;
import br.com.itau.Cartao.models.Pagamento;
import br.com.itau.Cartao.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    public Pagamento pagarItem(PagamentoEntradaDTO pagamentoEntradaDTO) {
        Pagamento pagamento = new Pagamento();
        Cartao cartao = cartaoService.buscarCartao(pagamentoEntradaDTO.getCartao_id());

        pagamento.setCartao(cartao);
        pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
        pagamento.setValor(pagamentoEntradaDTO.getValor());

        return pagamentoRepository.save(pagamento);

    }

    public List<Pagamento> listarPagamentos(int id) {
        Pagamento pagamento = new Pagamento();
        Cartao cartao = cartaoService.buscarCartao(id);

       return pagamentoRepository.findAllByCartao(cartao);
    }
}
